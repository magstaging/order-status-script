<?php
/**
 * Created by PhpStorm.
 * User: herve.tribouilloy
 * Date: 27/01/20
 * Time: 11:36
 */

namespace Mbs\OrderStatus\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class ApproveOrders extends Command
{
    /**
     * @var \Mbs\OrderStatus\Model\OrderApprovalHandler
     */
    private $approvalHandler;

    public function __construct(
        \Mbs\OrderStatus\Model\OrderApprovalHandler $approvalHandler,
        ?string $name = null
    ) {
        parent::__construct($name);
        $this->approvalHandler = $approvalHandler;
    }

    protected function configure()
    {
        $this->setName('order:status:approve')
            ->setDescription('Update orders status to approve');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $result = $this->approvalHandler->changeUnprovedOrderStatusToApproved();

        if (!empty($result)) {
            foreach ($result as $orderChanged) {
                $output->writeln(sprintf('The order %s was changed', $orderChanged));
            }
        }
    }
}