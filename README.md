## Order Status Script

The module changes the orders that are with an unapproved status to an approved status in batch

Right now, it changes the order with the canceled status to the pending status.

If the script has to change orders status using custom status, these need to be added in the Magento backend. 

- clone the repository
- create folder app/code/Mbs/OrderStatus when located at the root of the Magento site
- copy the content of this repository within the folder
- install the module php bin/magento setup:upgrade

- Go to Stores/Order Status
- Add an unapproved status
- Add an approved status

- change the Model in this module to adjust the unapproved/approved order status codes to use

the command-line will change the order status for the orders that have the unapproved status.
php bin/magento order:status:approve 

