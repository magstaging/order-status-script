<?php
/**
 * Created by PhpStorm.
 * User: herve.tribouilloy
 * Date: 27/01/20
 * Time: 11:39
 */

namespace Mbs\OrderStatus\Model;

use Magento\Eav\Model\Entity\Collection\AbstractCollection;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Sales\Model\OrderRepository;

class OrderApprovalHandler
{
    private static $unapprovedStatus = 'canceled'; //'disapproved';

    private static $approvedStatus = 'pending'; //'approved';

    /**
     * @var OrderRepository
     */
    private $orderRepository;
    /**
     * @var \Magento\Framework\Api\FilterBuilder
     */
    private $filterBuilder;
    /**
     * @var \Magento\Framework\Api\Search\SearchCriteriaBuilder
     */
    private $searchCriteriaBuilder;

    public function __construct(
        OrderRepository $orderRepository,
        \Magento\Framework\Api\FilterBuilder $filterBuilder,
        \Magento\Framework\Api\Search\SearchCriteriaBuilder $searchCriteriaBuilder
    ) {
        $this->orderRepository = $orderRepository;
        $this->filterBuilder = $filterBuilder;
        $this->searchCriteriaBuilder = $searchCriteriaBuilder;
    }

    public function changeUnprovedOrderStatusToApproved()
    {
        $result = [];

        try {
            $orders = $this->findUnprovedOrders();

            if (!empty($orders)) {
                foreach ($orders as $order) {
                    $order->setStatus(self::$approvedStatus);
                    $this->orderRepository->save($order);

                    $result[] = $order->getIncrementId();
                }
            }
        } catch (NoSuchEntityException $e) {
            // send an user friendly error
        }

        return $result;
    }

    /**
     * @return \Magento\Sales\Api\Data\OrderInterface[]
     * @throws NoSuchEntityException
     */
    private function findUnprovedOrders()
    {
        $filter = $this->filterBuilder
            ->setField('status')
            ->setConditionType('eq')
            ->setValue(self::$unapprovedStatus)
            ->create();

        $this->searchCriteriaBuilder->addSortOrder('entity_id', AbstractCollection::SORT_ORDER_DESC);
        $this->searchCriteriaBuilder->addFilter($filter);
        //$this->searchCriteriaBuilder->setPageSize(2);
        $searchCriteria = $this->searchCriteriaBuilder->create();

        // quotes and quote addresses
        $orders = $this->orderRepository->getList($searchCriteria);
        if ($orders->getTotalCount()==0) {
            throw new NoSuchEntityException(__('No Order is found with the status ' . self::$unapprovedStatus));
        }

        return $orders->getItems();
    }
}
